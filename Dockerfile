FROM node:21.4.0-alpine3.17

ENV RENOVATE_VERSION=37.440.7

WORKDIR /renovate

RUN npm install renovate@${RENOVATE_VERSION} && apk add --no-cache git re2 go

CMD ["/renovate/node_modules/.bin/renovate"]
